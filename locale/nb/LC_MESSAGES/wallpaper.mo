��          �   %   �      p  *   q     �  	   �     �     �     �     �     �     �     �     �     �     �     �          "     (     6     D     U     e     t  	   �     �     �  Z   �       �       �     �  
   �     �  
   �     �     �                              2     ?  )   X     �  
   �  
   �     �     �  
   �     �  
   �     �     �  Y        k                                                                                         	   
                                 Time between wallpaper cycle (Minutes)   About All Files Apply Centre Close Default Folder Error Fill Help Images No Wallpaper Options Random Wallpaper Random Wallpaper Timed Scale Select Colour Select Folder Select Folder... Select Image... Select Picture Select background colour Set Image Static Successfully updated This is an antiX application for setting the wallpaper on the preinstalled window managers antiX Wallpaper Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2013-07-06 14:51+0000
Last-Translator: heskjestad <cato@heskjestad.xyz>, 2021
Language-Team: Norwegian Bokmål (http://www.transifex.com/anticapitalista/antix-development/language/nb/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nb
Plural-Forms: nplurals=2; plural=(n != 1);
 Tidsrotering (minutter) Om Alle filer Bruk Sentrering Lukk Standard filmappe Feil Fyll Hjelp Bilder Intet bakgrunnsbilde Alternativer Tilfeldig bakgrunnsbilde Tidsrotering av tilfeldig bakgrunnsbilde  Tilpass størrelse Velg farge Velg mappe Velg mappe … Velg bilde … Velg bilde Velg bakgrunnsfarge Bruk bilde Statisk Oppdatering fullført Dette er et antiX-program for å velge bakgrunn på de forhåndsinstallerte skrivebordene antiX bakgrunnsbilde 