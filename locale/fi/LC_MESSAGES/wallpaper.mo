��          �   %   �      p  *   q     �  	   �     �     �     �     �     �     �     �     �     �     �     �          "     (     6     D     U     e     t  	   �     �     �  Z   �       �    (   �     �     �     �  
   �     
            	   #     -     2     8  	   G     Q      h  	   �     �     �     �     �     �     �  
   �  
   �     	  U   #     y                                                                                         	   
                                 Time between wallpaper cycle (Minutes)   About All Files Apply Centre Close Default Folder Error Fill Help Images No Wallpaper Options Random Wallpaper Random Wallpaper Timed Scale Select Colour Select Folder Select Folder... Select Image... Select Picture Select background colour Set Image Static Successfully updated This is an antiX application for setting the wallpaper on the preinstalled window managers antiX Wallpaper Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2013-07-06 14:51+0000
Last-Translator: Kimmo Kujansuu <mrkujansuu@gmail.com>, 2021
Language-Team: Finnish (http://www.transifex.com/anticapitalista/antix-development/language/fi/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fi
Plural-Forms: nplurals=2; plural=(n != 1);
   Aika taustakuvan välillä (minuuttia) Tietoja Kaikki tiedostot Hyväksy Keskitetty Sulje Oletuskansio Virhe Täytetty Ohje Kuvat Ei taustakuvaa Asetukset Satunnainen taustakuva Satunnainen taustakuva ajastettu Skaalattu Valitse väri Valitse kansio Valitse kansio... Valitse kuva... Valitse kuva Valitse taustaväri Aseta kuva Staattinen Päivitetty onnistuneesti Tämä on antiX-sovellus taustakuvan asettamiseksi esiasennettuun ikkunointiohjelmaan Taustakuva antiX 