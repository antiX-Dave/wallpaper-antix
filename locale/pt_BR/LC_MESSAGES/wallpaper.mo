��          �   %   �      p  *   q     �  	   �     �     �     �     �     �     �     �     �     �     �     �          "     (     6     D     U     e     t  	   �     �     �  Z   �       �    3   �     3     9     K     S     Z     a     o  	   t     ~     �     �     �     �  &   �     �     �               )     ?     R     k  	   |     �  {   �                                                                                              	   
                                 Time between wallpaper cycle (Minutes)   About All Files Apply Centre Close Default Folder Error Fill Help Images No Wallpaper Options Random Wallpaper Random Wallpaper Timed Scale Select Colour Select Folder Select Folder... Select Image... Select Picture Select background colour Set Image Static Successfully updated This is an antiX application for setting the wallpaper on the preinstalled window managers antiX Wallpaper Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2013-07-06 14:51+0000
Last-Translator: marcelo cripe <marcelocripe@gmail.com>, 2020-2021
Language-Team: Portuguese (Brazil) (http://www.transifex.com/anticapitalista/antix-development/language/pt_BR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt_BR
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Tempo entre o ciclo do papel de parede (em minutos) Sobre Todos os Arquivos Aplicar Centro Fechar Pasta Padrão Erro Preencher Ajuda Imagens Sem Papel de Parede Opções Papel de Parede Aleatório Papel de Parede Aleatório Temporizado Escala Selecione a Cor Selecione a Pasta Selecione a Pasta... Selecione a Imagem... Selecione a Imagem Selecione a cor do fundo Definir a Imagem Estático Atualizado com sucesso Este é um aplicativo do antiX para definir o papel de parede / imagem de fundo nos gerenciadores de janela pré-instalados Papel de Parede do antiX 