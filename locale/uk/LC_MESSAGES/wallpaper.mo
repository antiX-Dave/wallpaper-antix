��          �   %   �      p  *   q     �  	   �     �     �     �     �     �     �     �     �     �     �     �          "     (     6     D     U     e     t  	   �     �     �  Z   �       x    =   �     �     �     �               +     I     X     m     |     �  
   �  !   �  -   �                '  "   C  (   f     �  "   �  )   �     �     	  �   '	     �	                                                                                         	   
                                 Time between wallpaper cycle (Minutes)   About All Files Apply Centre Close Default Folder Error Fill Help Images No Wallpaper Options Random Wallpaper Random Wallpaper Timed Scale Select Colour Select Folder Select Folder... Select Image... Select Picture Select background colour Set Image Static Successfully updated This is an antiX application for setting the wallpaper on the preinstalled window managers antiX Wallpaper Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2013-07-06 14:51+0000
Last-Translator: Tymofii Lytvynenko <till.svit@gmail.com>, 2022
Language-Team: Ukrainian (http://www.transifex.com/anticapitalista/antix-development/language/uk/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: uk
Plural-Forms: nplurals=4; plural=(n % 1 == 0 && n % 10 == 1 && n % 100 != 11 ? 0 : n % 1 == 0 && n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 12 || n % 100 > 14) ? 1 : n % 1 == 0 && (n % 10 ==0 || (n % 10 >=5 && n % 10 <=9) || (n % 100 >=11 && n % 100 <=14 )) ? 2: 3);
   Час між циклами шпалер (хвилини)  Про Всі файли Застосувати Центрування Закрити Типовий каталог Помилка Заповнення Довідка Зображення Без шпалер Опції Випадкові шпалери Випадкові шпалери на час Масштаб Обрати колір Вибір каталогу Виберіть каталог... Виберіть зображення... Вибір каталогу Виберіть колір тла Установити зображення Статика Успішно оновлено Це antiX програма для налаштування шпалер на попередньо встановлених віконних менеджерах Шпалери antiX 