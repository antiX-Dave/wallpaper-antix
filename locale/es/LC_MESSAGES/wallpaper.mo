��          �   %   �      p  *   q     �  	   �     �     �     �     �     �     �     �     �     �     �     �          "     (     6     D     U     e     t  	   �     �     �  Z   �       �    +   �  	   �     �                         /     5     <  	   B     L     \     e  ,   {     �     �     �     �     �               1  	   C     M  i   h     �                                                                                         	   
                                 Time between wallpaper cycle (Minutes)   About All Files Apply Centre Close Default Folder Error Fill Help Images No Wallpaper Options Random Wallpaper Random Wallpaper Timed Scale Select Colour Select Folder Select Folder... Select Image... Select Picture Select background colour Set Image Static Successfully updated This is an antiX application for setting the wallpaper on the preinstalled window managers antiX Wallpaper Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2013-07-06 14:51+0000
Last-Translator: Amigo, 2022
Language-Team: Spanish (http://www.transifex.com/anticapitalista/antix-development/language/es/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Tiempo entre ciclo de papel tapiz (minutos) Acerca de Todos los archivos Aplicar Centrar Cerrar Carpeta por defecto Error Llenar Ayuda Imágenes Sin papel tapiz Opciones Papel tapiz aleatorio Papel tapiz aleatorio con tiempo específico Escalar Seleccionar color Seleccionar carpeta Seleccionar carpeta... Seleccionar imagen... Seleccionar imagen Seleccionar color de fondo Establecer imagen Estático Se actualizó exitosamente Esta aplicación de antiX le permite seleccionar su papel tapiz en los gestores de ventanas preinstalados Papel tapiz antiX 